<?php

use Illuminate\Database\Seeder;

class EmployeesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                 'name' => 'aaa',
                 'email' =>'aaa@aaa.aaaa',
                 'password' => Hash::make('12345'),
                 'created_at' => date('Y-m-d G:i:s'),
                 'role'=>'employee',
            ],
 
            [
                 'name' => 'bbb',
                 'email' =>'bbb@bbb.bbbb',
                 'password' => Hash::make('12345'),
                 'created_at' => date('Y-m-d G:i:s'),
                 'role'=>'employee',
                ],
                 ]);
     }
    }

