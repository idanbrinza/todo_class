<h1>Edit Existing Todo</h1>
<form method = 'POST' action ="{{action('TodoController@update', $todo->id)}}">
@csrf
@method('PATCH')
<div class = "form-group">
<label for = "title">Todo to Update:</label>
<input type = "text" class= "form-control" name = "title" value = "{{$todo->title}}">
</div>

<div class = "form-group">
<input type = "submit" class= "form-control" name = "submit" value = "Save">
</div>
</form>


<form method = 'POST' action ="{{action('TodoController@destroy', $todo->id)}}">
@csrf
@method('DELETE')
<div class = "form-group">
<input type = "submit" class= "form-control" name = "submit" value = "Delete">
</div>

</form>
